/*
  FILE: APConfiguration.h

  Configuration of the Access Point
  Definitions and Declarations
*/

#ifndef _AP_CONFIGURATION_
#define _AP_CONFIGURATION_

#include <Arduino.h>
// default values definition

#define MQTT_DEFAULT_PORT "1883"
#define MQTT_DEFAULT_SERVER ""
#define STATIC_IP "10.0.1.56"
#define GATEWAY "10.0.1.1"
#define NETWORK_MASK "255.255.255.0"

#define MQTT_SERVER_JSON_FIELD "mqtt_server"
#define MQTT_PORT_JSON_FIELD "mqtt_port"
#define MQTT_USERNAME_JSON_FIELD "mqtt_user"
#define MQTT_PASSWORD_JSON_FIELD "mqtt_password"

#define _DEFAULT_CONFIG_NAME_  "/config.json"

struct TaskConfiguration{
 
      char mqttServer[40];
      char mqttPort[6];
      char mqttUsername[40];
      char mqttPassword[40];
      char staticIp[16];
      char staticGw[16];
      char staticSn[16];
  
      TaskConfiguration( const char* server, 
        const char* port, 
        const char* staticIP, 
        const char* gateway,
        const char* subNetwork);
      
      TaskConfiguration( TaskConfiguration& fromConfig);
    // Load configuration from a file placed in the File System
      bool loadConfigFile(const char* fileName = NULL);
    } ;

#endif