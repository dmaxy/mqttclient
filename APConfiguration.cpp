/*
  FILE: APConfiguration.cpp

  Configuration of the Access Point
  Implementation
*/



#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson

#include <FS.h>  
#include "APConfiguration.h"

TaskConfiguration::TaskConfiguration( const char* server, 
    const char* port, 
    const char* staticIP,
    const char* gateway,
    const char* subNetwork){
    strcpy(this->mqttServer, server);
    strcpy(this->mqttPort, port);
    strcpy(this->staticIp, staticIP);
    strcpy(this->staticGw, gateway);
    strcpy(this->staticSn, subNetwork);
};

TaskConfiguration::TaskConfiguration( TaskConfiguration& fromConfig){
    strcpy(this->mqttServer, fromConfig.mqttServer);
    strcpy(this->mqttPort, fromConfig.mqttPort);
    strcpy(this->staticIp, fromConfig.staticIp);
    strcpy(this->staticGw, fromConfig.staticGw);
    strcpy(this->staticSn, fromConfig.staticSn);
};

bool TaskConfiguration::loadConfigFile(const char* fileName){

    

    char* configFileName = _DEFAULT_CONFIG_NAME_ ;

    if (fileName != NULL){
        configFileName = (char*)fileName;
    }

    Serial.println("Start Config Loading for filename: " + String(configFileName) );

    bool result = false;

    if (SPIFFS.exists(configFileName)) {
         //file exists, reading and loading
         File configFile = SPIFFS.open(configFileName, "r");
         if (configFile) {
           Serial.println("opened config file");
           size_t size = configFile.size();
           // Allocate a buffer to store contents of the file.
           std::unique_ptr<char[]> buf(new char[size]);
   
           char* bufferPointer = buf.get();
           
           configFile.readBytes(bufferPointer, size);
           DynamicJsonBuffer jsonBuffer;
           JsonObject& json = jsonBuffer.parseObject(bufferPointer);
           json.printTo(Serial);
           if (json.success()) {
             Serial.println("\nparsed json");
             result = true;
             strcpy(this->mqttServer, json[ MQTT_SERVER_JSON_FIELD ]);
             strcpy(this->mqttPort, json[ MQTT_PORT_JSON_FIELD ]);
             strcpy(this->mqttUsername, json[ MQTT_USERNAME_JSON_FIELD ]);
             strcpy(this->mqttPassword, json[ MQTT_PASSWORD_JSON_FIELD ]);
             //strcpy(blynk_token, json["blynk_token"]);
   
             if(json["ip"]) {
               Serial.println("setting custom ip from config");
               //static_ip = json["ip"];
               strcpy(this->staticIp, json["ip"]);
               strcpy(this->staticGw, json["gateway"]);
               strcpy(this->staticSn, json["subnet"]);
   
               Serial.println("Static ip:" + String(this->staticIp));
   /*          Serial.println("converting ip");
               IPAddress ip = ipFromCharArray(static_ip);
               Serial.println(ip);*/
             } else {
               Serial.println("no custom ip in config");
             }
           } else {
             Serial.println("failed to load json config");
           }
         }
       }
     return result;
    }
   