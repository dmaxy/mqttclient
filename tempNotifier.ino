
#include <dht.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h> 
#include <ESP8266WebServer.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <FS.h>
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson

#include "mqttmanager.h"
#include "APConfiguration.h"
 
//const char* ssid = "Keenetic-3384";
//const char* password = "JaEYW6tD";

TaskConfiguration configuration(MQTT_DEFAULT_SERVER, 
                                  MQTT_DEFAULT_PORT, 
                                          STATIC_IP, 
                                            GATEWAY, 
                                       NETWORK_MASK);

//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

WiFiManager wifiManager;

#define DHT22_PIN 5 //GPIO 5 (D1)

 dht DHT;
  // const char* mqtt_server = "192.168.5.9";
  // const char* mqttServer = "m20.cloudmqtt.com";
  // int mqttPort =  26229;
  // const char* mqttUser = "dmaxy";
  // const char* mqttPassword = "l13169";
  const char* publishTopic = "dacha/sensors/1";
  //const char* 

  void callback(char* topic, byte* payload, unsigned int length);
  bool loadCertificate(const char* fileName, File& ca);

  MqttManager* manager = NULL;

  //void setup_wifi();

  void setup() {
  // put your setup code here, to run once:

  delay(2000);
  Serial.begin(115200);
  Serial.println("===== DHT + WiFi MANAGER TEST PROGRAM ====");
  Serial.println("LIBRARY VERSION: " + String(DHT_LIB_VERSION));
  Serial.println("==========================================");
  Serial.printf(" ESP8266 Chip id = %08X\n", ESP.getChipId());
  

  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
  }

  /*
 * Show list of files in FS
 */
#if 0
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
  Serial.print(dir.fileName());
  Serial.print(" : ");
  File f = dir.openFile("r");
  Serial.println(f.size());
  }
#endif

#if 0
  if (SPIFFS.exists(_DEFAULT_CONFIG_NAME_)){
    if(SPIFFS.remove(_DEFAULT_CONFIG_NAME_)){
      Serial.println("Removed Config file");
    }
  }
#endif

  File ca;
  if (!loadCertificate("/caroot.crt", ca)){
    Serial.println("Warning: Cannot load certificate");
    return;
  }
  
  if(!configuration.loadConfigFile()){
    Serial.println("Warning: Cannot load config file");
  }
  
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  // set the additional metainfomration
  //wifiManager.setCustomHeadElement("<meta charset=\"utf-8\"/>");
  // JSON field name, "Form Filed Name", "Default Value", "Field Length"
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", configuration.mqttServer, 41);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", configuration.mqttPort, 6);
  WiFiManagerParameter custom_mqtt_username("mqttuser", "mqtt username", configuration.mqttUsername, 41);
  WiFiManagerParameter custom_mqtt_password("mqttpassword", "mqtt password", configuration.mqttPassword, 41);
  WiFiManagerParameter label("Use Static IP");
  WiFiManagerParameter custom_is_static_ip("isStatic", "is static", "true", 6,  "type=\"checkbox\"");

   //set static ip for client station mode instead of DHCP
   /*IPAddress _ip,_gw,_sn;
   _ip.fromString(configuration.staticIp);
   _gw.fromString(configuration.staticGw);
   _sn.fromString(configuration.staticSn);*/

  //wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);
  
  //add all your parameters here
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  //wifiManager.addParameter(&custom_is_static_ip);
  wifiManager.addParameter(&custom_mqtt_username);
  wifiManager.addParameter(&custom_mqtt_password);
  wifiManager.addParameter(&label);
  wifiManager.addParameter(&custom_is_static_ip);
  
  //reset settings
  // uncomment for testing
  //wifiManager.resetSettings();

  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  //wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("AutoConnectAP", "password")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  //read updated parameters
  strcpy(configuration.mqttServer, custom_mqtt_server.getValue());
  strcpy(configuration.mqttPort, custom_mqtt_port.getValue());
  strcpy(configuration.mqttUsername, custom_mqtt_username.getValue());
  strcpy(configuration.mqttPassword, custom_mqtt_password.getValue());

  //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json[ MQTT_SERVER_JSON_FIELD ] = configuration.mqttServer;
    json[ MQTT_PORT_JSON_FIELD ] = configuration.mqttPort;
    json[MQTT_USERNAME_JSON_FIELD] = configuration.mqttUsername;
    json[MQTT_PASSWORD_JSON_FIELD] = configuration.mqttPassword;

/*
    json["ip"] = WiFi.localIP().toString();
    json["gateway"] = WiFi.gatewayIP().toString();
    json["subnet"] = WiFi.subnetMask().toString();*/

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.prettyPrintTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
  }

  // Serial.println("local ip");
  // Serial.println(WiFi.localIP());
  // Serial.println(WiFi.gatewayIP());
  // Serial.println(WiFi.subnetMask());


  //replace ca.crt eith your uploaded file name

  // if (wifiManager.connected()){


  // }
  
manager = new MqttManager(/*ssid, 
                    password, */
                    configuration.mqttServer, 
                    atoi(configuration.mqttPort),
                          callback, 
                         NULL,
                         configuration.mqttUsername, 
                         configuration.mqttPassword, 
                         &ca);
 if(!manager){
     Serial.println("Warning: Manager is not initialized");
     return;  
 }
//  
//  manager->setupWifi();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String strPayload = "";
  String strTopic(topic);
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    strPayload += (char)payload[i];
  }
  //Serial.println();
  Serial.println("Message Payload: " + strPayload);
  }

void loop() {

  if (manager == NULL) {
      return;
  }
  
  // put your main code here, to run repeatedly:

  if (!manager->connected()) {
    manager->reconnect();
  }
      
  manager->loop();

  int chk = DHT.read22(DHT22_PIN);
  String message = "";
  
  switch (chk)
  {
    case DHTLIB_OK:  
      Serial.print("OK,\t");
      Serial.print(DHT.humidity, 1);
      Serial.print(",\t");
      Serial.println(DHT.temperature, 1);
      message += "{\n\t\ \"sensorId\":\"" + manager->clientId() + 
        "\", \n\t\"temperature\":" + DHT.temperature + 
        ", \n\t \"humidity\":" + DHT.humidity + "\n}";
      manager->publish(publishTopic, message.c_str());
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      Serial.println("Checksum error,\t"); 
      manager->publish(publishTopic, "{ \"error\" : \"DHT Checksum error.\"\n}");
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      Serial.println("Time out error,\t"); 
      manager->publish(publishTopic, "{ \"error\" : \"DHT Time out error.\"\n}");
      break;
    default: 
      Serial.println("Unknown error,\t"); 
      manager->publish(publishTopic, "{ \"error\" : \"DHT Unknown error.\"\n}");
      break;
  }
      
  

  delay(5000);
}

/**
 * load Cerfiticate 
 */
bool loadCertificate(const char* fileName, File& ca){

  Serial.println("Try to load the certificate");

  File _ca = SPIFFS.open("/caroot.crt", "r");
  bool result = false;
  if (!_ca) {
    Serial.println("Failed to open ca file");
  } else {
    Serial.println("Successed to open ca file");  
    ca = _ca;
    result = true;
  }

  return result;
}


 