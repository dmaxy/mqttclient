#include "mqttManager.h"
#include <assert.h>
#include <FS.h>

MqttManager::MqttManager(/*const char* ssid, 
                         const char* wifiPassword, */
                         const char* mqttServer,
                         const int mqttport, 
                         MQTT_CALLBACK_SIGNATURE,
                         const char* mqttClientPrefix,
                         const char* mqttUser, 
                         const char* mqttPassword,
                         File* CAFile){
                          
  //this->ssid = ssid;
  //this->wifiPassword = wifiPassword;
  this->mqttServer = mqttServer;
  this->mqttUser = mqttUser;
  this->mqttPassword = mqttPassword;
  this->mqttPort = mqttport;
  this->caFile = CAFile;
  
  //this->wifiClient = new WiFiClient;
  this->sslClient = new WiFiClientSecure;
  
  
    
  //this->pubSubClient = new PubSubClient(*wifiClient);
  this->pubSubClient = new PubSubClient(*sslClient);
  
  if( sslClient->loadCertificate(*caFile) )
    Serial.println("Certificate loaded");
  else
    Serial.println("Certificate not loaded");
  
  if (mqttClientPrefix == NULL){
    this->clientUid = "EspClient_";
    } else {
      this->clientUid = mqttClientPrefix;
      }
      this->clientUid += createClientSuffix();
  if (callback == NULL){
    assert(false);
    Serial.println ("mqtt callback can't be NULL");
    }
  this->callback = callback;

  pubSubClient->setServer(this->mqttServer, this->mqttPort);
  if(this->callback != NULL)
    pubSubClient->setCallback(this->callback);
}

MqttManager::~MqttManager(){
    delete pubSubClient;
    delete sslClient;
}

bool MqttManager::connect(int& error_code){
    error_code = 0;
    return false;
}
/*
void MqttManager::setupWifi(){
  
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(this->ssid);
  Serial.print("pass:");
  Serial.println(this->wifiPassword);
  WiFi.begin(this->ssid, this->wifiPassword);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);

  this->wifiConnected = true;
  Serial.print("Set server: ");
  Serial.println(mqttServer);
  Serial.println("Set port: " + String(this->mqttPort));
  pubSubClient->setServer(this->mqttServer, this->mqttPort);
  if(this->callback != NULL)
    pubSubClient->setCallback(this->callback);
 }
*/

 String MqttManager::createClientSuffix(){

  //   WiFi.macAddress(this->mac);
  
  // String macAddress = "";
  // int i = 0;
  // for (i; i<6; i++){
  //   macAddress += String(mac[i],HEX);
  //   }
  // macAddress += String(mac[i],HEX);
  // //return macAddress;
    char idString[9];
    sprintf(idString, "%08X", ESP.getChipId());
    
    return String(idString);
  }

  void MqttManager::reconnect(){
     while ( !pubSubClient->connected() ) {
      Serial.println("Attempting MQTT connection to " + String(this->mqttServer) + ":" + String(this->mqttPort));
      Serial.println("Client Id: " + String(clientUid));
      Serial.println("Username: " + String(mqttUser));
      Serial.println("Password: " + String(mqttPassword));
      
      // Attempt to connect
 
      if ( pubSubClient->connect(
           clientUid.c_str(), 
           mqttUser, 
           mqttPassword ) ) {
        Serial.println("connected");
  
        delay(100);
 
        // Once connected, publish an announcement...
        pubSubClient->publish("/test/status/1", "Reconnect to MQTT broker");
        // ... and resubscribe
      } else {
        Serial.print("failed, rc=");
        Serial.print(pubSubClient->state());
        Serial.println(" try again in 5 seconds");
        // Wait 5 seconds before retrying
        delay(5000);
      }
    }
  }

  void MqttManager::loop(){
    pubSubClient->loop();
  }

  boolean MqttManager::publish(const char* topic, const char* payload){
    pubSubClient->publish(topic, payload);
  }

  boolean MqttManager::connected(){
    return pubSubClient->connected();
  }

  // TaskConfiguration::TaskConfiguration(){
  //   //this->mqttServer = NULL;
  // }