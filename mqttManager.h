/*
  FILE: mqttmanager.h

  manager of mqtt connection

*/

#ifndef _MQTT_MANAGER_
#define _MQTT_MANAGER_

//#include <Arduino.h>
//#include <WiFi.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiClientSecure.h>
#include <FS.h>


class MqttManager {

private:

  //const char* ssid;
  //const char* wifiPassword;
  const char* mqttServer;
  int mqttPort;
  MQTT_CALLBACK_SIGNATURE;
  const char* mqttUser;
  const char* mqttPassword;
  //byte mac[6];
  String clientUid;
  //WiFiClient* wifiClient;
  WiFiClientSecure* sslClient;
  PubSubClient* pubSubClient;
  File* caFile; 

  bool wifiConnected;
  bool mqttConnected;
// generate unique MQTT client ID
  String createClientSuffix();

public:
  MqttManager(/*const char* ssid, 
            const char* wifiPassword, */
            const char* mqttServer,
            const int mqttport,
            MQTT_CALLBACK_SIGNATURE,
            const char* mqqtClientPrefix = NULL,
            const char* mqttUser = NULL, 
            const char* mqttPassword = NULL,
            File* CAFile = NULL);
            String clientPrefix;

  ~MqttManager();

 // atempt to connect to fi
 bool connect(int& error_code);
// setup Wifi connection
 void setupWifi();

 inline String clientId(){
    return clientUid;
 }

 void reconnect();

 void loop();

 boolean publish(const char* topic, const char* payload);

 boolean connected();
 };

 

#endif
